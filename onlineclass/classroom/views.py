from django.shortcuts import render, redirect
from .models import Classroom
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.conf import settings
from django.contrib import messages

# Create your views here.
def home(request):
    """
    Url to home page.

    :http relative-url: /
    :http-method : GET
    """
    classes = Classroom.objects.all()
    context = {'classes': classes}
    template = 'home.html'
    return render(request, template, context)

def class_details(request, id):
    """
    Url to class detail page.

    :http relative-url: /classroom/<id>
    :http-method : GET
    """
    class_details = Classroom.objects.get(id=id)
    template = 'class_details.html'
    context = {'class_details':class_details}
    return render(request, template, context)

@login_required(login_url="/student/login/")
def register(request, id):
    """
    Url to register to a classroom.

    :http relative-url: /classroom/<id>
    :http-method : POST
    """
    user = request.user
    classroom = Classroom.objects.get(id=id)
    if classroom:
        if user not in classroom.student_set.all():
            user.student.classes.add(classroom)
            send_mail('Registration Successful', 'Dear '+user.student.name+', you have successfully registered to '+classroom.room_name+' course. Please find the details from the website.',
            settings.EMAIL_HOST_USER,[user.email],fail_silently=False
            )
            messages.success(request, "Registration Successful")
        else:
            messages.error(request, "ALready registered to this course")
    else:
        messages.error(request, "Could not find the classroom you were looking for.")
    return redirect('student:my_classes')

@login_required(login_url="/student/login/")
def register_cancel(request, id):
    """
    Url to cancel registration.
    :http relative-url: /classroom/<id>
    :http-method : POST
    """

    user = request.user
    classroom = Classroom.objects.get(id=id)
    if classroom:
        user.student.classes.remove(classroom)
        send_mail('Registration Cancelled', 'Dear '+user.student.name+', you have successfully registered to '+classroom.room_name+' course. Please find the details from the website.',
            settings.EMAIL_HOST_USER,[user.email],fail_silently=False
            )
        messages.success(request, "Registration cancelled")
    else:
        messages.error(request, "Could not find the classroom you were looking for.")
    return redirect('student:my_classes')