from django import forms
from .models import Student
from classroom.models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError

class StudentAccountCreationForm(UserCreationForm):
    email = forms.EmailField(required=True)
    class Meta:
        model = User
        fields = ['email','username']

    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_student = True
        user.save()
        student = Student.objects.create(user=user)
        student.email = self.cleaned_data["email"]
        student.name = self.cleaned_data["username"]
        student.password = self.cleaned_data["password1"]
        student.save()
        return student

    def clean_username(self):
        name = self.cleaned_data['username'].lower()
        student = Student.objects.filter(name=name)
        if student.count():
            raise  ValidationError("Username already exists")
        return name

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        if not email:
            raise  ValidationError("Email field cannot be empty")
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise ValidationError("Password don't match")
        return password2
