from django.db import models
from django.contrib.auth.models import AbstractUser
from classroom.models import Classroom
from django.conf import settings

# Create your models here.

# class User(AbstractUser):

#     is_student = models.BooleanField(default=False)
#     is_teacher = models.BooleanField(default=False)


class  Student(models.Model):

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    email = models.EmailField()
    name = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
    classes = models.ManyToManyField(Classroom)
    
    class Meta:
        verbose_name = 'Student'
        verbose_name_plural = 'Students'

    def __str__(self):
         return self.email