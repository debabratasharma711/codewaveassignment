from django.shortcuts import render, redirect
from .forms import StudentAccountCreationForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.conf import settings
from django.contrib import messages

# Create your views here.
def signup_view(request):
    """
    Url to user's sign up page.

    :http relative-url: /student/signup
    :http-method : POST, GET
    """
    if request.method== "POST":
        form = StudentAccountCreationForm(request.POST)
        if form.is_valid():
            student = form.save()
            send_mail('Welcome to Shikshalay','Hello '+student.name+', your account has been successfully created',
                settings.EMAIL_HOST_USER, [student.email], fail_silently=False
                )
            messages.success(request, "Account created Successfully")
            return redirect('student:login_view')
        

    else:
        form = StudentAccountCreationForm
        title = 'Sign Up'
        context = {'form':form, 'title': title}
        return render(request, 'signup.html',context)


def login_view(request):

    """
    Url to user's login page.

    :http relative-url: /student/login
    :http-method : POST, GET
    """
    if request.user.is_authenticated:
        return redirect('cr:home')

    else:
    
        if request.method == "POST":
            form = AuthenticationForm(data=request.POST)
            if form.is_valid():
                user = form.get_user()
                try:
                    login(request, user)
                    messages.success(request, "Logged in successfully")
                    if 'next' in request.POST:
                        return redirect(request.POST.get('next'))
                    else:
                        return redirect('cr:home')
                except Exception as e:
                    print(e)
                    
            else:
                messages.error(request, 'Please check the credentials and log in again')
                form = AuthenticationForm()
                context = {'form':form, 'title':'login' }
                return render(request, 'login.html', context)
        else:
            form = AuthenticationForm()
            context = {'form':form, 'title':'login' }

        return render(request, 'login.html', context)

@login_required(login_url="/student/login/")
def logout_view(request):
    """
    Url to user's logout.

    :http relative-url: /student/logout
    :http-method : GET
    """
    logout(request)
    messages.success(request, "Logged out")
    return redirect('cr:home')

@login_required(login_url="/student/login/")
def my_classes(request):
    """
    Url to user's registered classes.

    :http relative-url: /student/my_classes
    :http-method : GET
    """
    user = request.user
    my_class = user.student.classes.all() 
    context = {'my_classes':my_class}
    template = 'my_classes.html'
    return render(request, template, context)